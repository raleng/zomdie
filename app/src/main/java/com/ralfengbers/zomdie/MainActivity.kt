package com.ralfengbers.zomdie

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

import kotlinx.android.synthetic.main.main_activity.*
import kotlin.math.pow


class Variables(private val diceView: TextView,
                private val accView: TextView,
                val expView: TextView,
                val resultsView: RecyclerView) {

    var dice: Int = 2
        set(value) {
            field = value
            setDiceNum()
        }

    var acc: Int = 5
        set(value) {
            field = value
            setAccNum()
        }

    init {
        setDiceNum()
        setAccNum()
    }

    private fun setDiceNum() { diceView.text = dice.toString() }

    private fun setAccNum() {
        when (acc) {
            1, 2, 3, 4, 5 -> accView.text = "%d+".format(acc)
            else -> accView.text = acc.toString()
        }
    }

    fun calculateOdds(): MutableList<Float> {
        val winOdds = (7 - acc.toFloat()) / 6
        val loseOdds = 1 - winOdds
        val odds = mutableListOf<Float>()

        for (numWins in 1..dice) {
            var value = binomial(dice, numWins)
            value *= winOdds.pow(numWins)
            value *= loseOdds.pow(dice - numWins)
            odds.add(value)
        }
        return odds
    }

    fun cumulativeOdds(odds: MutableList<Float>): MutableList<Float> {
        val cumOdds = mutableListOf<Float>()

        for (i in 0..(odds.size-1)) {
            val value = odds.slice(i..(odds.size-1)).sum()*100
            cumOdds.add(value)
        }
        return cumOdds
    }

    fun expectedZombies(odds: MutableList<Float>): Float {
        var expValue: Float = 0.toFloat()
        for ((i, odd) in odds.withIndex()) {
            expValue += (i+1) * odd
        }
        return expValue
    }

    private fun calculateCrawler(dice: Int): MutableList<Float> {
        val crawlerOdds = mutableListOf<Float>()
        for (num in dice downTo 0) {
            val value = (1 - (5/6.toFloat()).pow(num)) * 100
            crawlerOdds.add(value)
        }
        crawlerOdds.add(0.toFloat())
        return crawlerOdds
    }

    private fun binomial(n: Int, k: Int): Float = (factorial(n) / (factorial(k) * factorial(n-k))).toFloat()

    private fun factorial(num: Int): Double {
        var result = 1.toDouble()
        for (i in 2..num) result *= i
        return result
    }
}


class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var case1: Variables
    private lateinit var case2: Variables
    private lateinit var cumOdds1: MutableList<Float>
    private lateinit var cumOdds2: MutableList<Float>

    private lateinit var results1View: RecyclerView
    private lateinit var results2View: RecyclerView
    private lateinit var viewAdapterResults1: RecyclerView.Adapter<*>
    private lateinit var viewAdapterResults2: RecyclerView.Adapter<*>
    private lateinit var viewManagerResults1: RecyclerView.LayoutManager
    private lateinit var viewManagerResults2: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        case1 = Variables(dice1, acc1, expected1, results1)
        cumOdds1 = case1.cumulativeOdds(case1.calculateOdds())
        case2 = Variables(dice2, acc2, expected2, results2)
        cumOdds2 = case2.cumulativeOdds(case2.calculateOdds())


        dice1_up.setOnClickListener(this)
        dice1_down.setOnClickListener(this)
        acc1_up.setOnClickListener(this)
        acc1_down.setOnClickListener(this)
        dice2_up.setOnClickListener(this)
        dice2_down.setOnClickListener(this)
        acc2_up.setOnClickListener(this)
        acc2_down.setOnClickListener(this)

        viewManagerResults1 = LinearLayoutManager(this)
        viewManagerResults2 = LinearLayoutManager(this)

        viewAdapterResults1 = MyAdapter(cumOdds1)
        viewAdapterResults2 = MyAdapter(cumOdds2)

        results1View = findViewById<RecyclerView>(R.id.results1).apply {
            layoutManager = viewManagerResults1
            adapter = viewAdapterResults1
        }

        results2View = findViewById<RecyclerView>(R.id.results2).apply {
            layoutManager = viewManagerResults2
            adapter = viewAdapterResults2
        }

        updateOdds(case1, cumOdds1)
        updateOdds(case2, cumOdds2)

    }

    // check here:
    // https://developer.android.com/guide/components/activities/activity-lifecycle#kotlin

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.run {
            putInt("CASE1_DICE", case1.dice)
            putInt("CASE1_ACC", case1.acc)
            putInt("CASE2_DICE", case2.dice)
            putInt("CASE2_ACC", case2.acc)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState?.run {
            case1.dice = getInt("CASE1_DICE")
            case1.acc = getInt("CASE1_ACC")
            case2.dice = getInt("CASE2_DICE")
            case2.acc = getInt("CASE2_ACC")
        }
        updateOdds(case1, cumOdds1)
        updateOdds(case2, cumOdds2)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.dice1_up -> case1.dice = case1.dice + 1
            R.id.dice2_up -> case2.dice = case2.dice + 1
            R.id.acc1_up -> case1.acc = increaseToSix(case1.acc)
            R.id.acc2_up -> case2.acc = increaseToSix(case2.acc)
            R.id.dice1_down -> case1.dice = decreaseToOne(case1.dice)
            R.id.dice2_down -> case2.dice = decreaseToOne(case2.dice)
            R.id.acc1_down -> case1.acc = decreaseToOne(case1.acc)
            R.id.acc2_down -> case2.acc = decreaseToOne(case2.acc)
        }
        when (view.id) {
            R.id.dice1_up, R.id.dice1_down, R.id.acc1_up, R.id.acc1_down -> updateOdds(case1, cumOdds1)
            R.id.dice2_up, R.id.dice2_down, R.id.acc2_up, R.id.acc2_down -> updateOdds(case2, cumOdds2)
        }
    }

    private fun updateOdds(case: Variables, cumOdds: MutableList<Float>) {
        cumOdds.clear()
        val odds = case.calculateOdds()
        cumOdds.addAll(case.cumulativeOdds(odds))
        case.expView.text = "%.2f".format(case.expectedZombies(odds))
        case.resultsView.adapter.notifyDataSetChanged()
    }

    private fun increaseToSix(num: Int) = when (num < 6) {
        true -> num + 1
        false -> 6
    }

    private fun decreaseToOne(num: Int) = when (num > 1) {
        true -> num - 1
        false -> 1
    }
}
