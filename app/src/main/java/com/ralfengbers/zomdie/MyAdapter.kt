package com.ralfengbers.zomdie

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class MyAdapter(private val myDataset: MutableList<Float>) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val posView: TextView = v.findViewById(R.id.posKillProbView)
        val textView: TextView = v.findViewById(R.id.killProbView)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MyAdapter.ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.results_item_row, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.posView.text = (position+1).toString()
        holder.textView.text = "%.2f%%".format(myDataset[position])
    }

    override fun getItemCount() = myDataset.size

}